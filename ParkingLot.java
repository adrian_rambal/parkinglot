import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

public class ParkingLot {
    public static void main(String[] args) throws IOException {
        if (args.length < 1) {
            System.out.println("Usage: ParkingLot <input file 1> ... <input file n> ");
        }
        else {
            double means [] = new double[args.length];
            int aux = 0;
            for (int j = 0; j < args.length; ++j) {
                DataInputStream scanner = new DataInputStream(new FileInputStream(args[j]));
                double[] input = new double[12000 * 2];
                int i = 0;
                System.out.println("---------------------------------");
                System.out.println("reading data... from file: " + args[j]);
                while (scanner.available() > 0) {
                    input[i++] = scanner.readDouble();
                }
                System.out.println("data read!");
                Test t = new Test(input);
                t.initiateTest();
                int tmp = t.getParked();
                aux += tmp;
                means[j] = tmp;
                System.out.println(args[j] + " avg: " + tmp);
                System.out.println("---------------------------------");
            }
            double avgParked = aux / args.length;
            System.out.println();
            System.out.println("Average parked cars: " + avgParked + ", expected 3523.0");

            /* calculating sd */
            double sd = 0;
            for (int i = 0; i < args.length; ++i) {
                sd += (means[i] - avgParked)*(means[i] - avgParked);
            }
            sd = Math.sqrt(sd/args.length);
            String res = String.format("%.1f", sd);
            System.out.println("Average standard deviation: " + res + ", expected 21.9");
            double std = (avgParked-3523.0)/sd;
            res = String.format("%.2f", std);
            System.out.println("Standard normal variable obtained: " + res);

        }
    }

}
