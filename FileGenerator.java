import java.io.*;
import java.util.Random;

/**
 * Created by adrian on 26/04/2016.
 */
public class FileGenerator {

    public static void main(String args[]) throws IOException {
        if (args.length != 1) {
            System.out.println("Usage: FileGenerator <output file>");
        }
        else {
            System.out.println("Generating file ...");
            Random randomGen = new Random();
            DataOutputStream out = new DataOutputStream(new FileOutputStream(args[0]));
            int i = 0;
            for (i = 0; i < 12000; ++i) { // it is needed 12000 parking coordinates
                // coordinates x,y of the car
                double x = randomGen.nextDouble() * 100;
                double y = randomGen.nextDouble() * 100;
                /* nextDouble() method of Random Java class returns a double
                   between 0.0, 1.0
                 */
                out.writeDouble(x);
                out.writeDouble(y);
            }
            out.flush();
            System.out.println("File generated ..." + i + " coordinates generated");
        }
    }
}
