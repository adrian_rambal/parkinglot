import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;
import java.util.*;

public class Test {
	
	private Random randomGen;
	private List<Point> parkedCars;
	private int parked;
	private double[] input;
	
	public Test(double[] input) {
		parked = 0;
		parkedCars = new ArrayList<Point>();
		try {
			randomGen = new Random();
		}
		catch (Exception e) {
			// handle exception
		}
		this.input = input;
	}
	
	private boolean canPark(Point p)
	{
		for (Point parked: parkedCars) {
			if (distance(parked,p) <= 1) return false;
		}
		return true;
		
	}

	private double distance(Point p1, Point p2)
	{
		return Math.max(Math.abs(p1.getX() - p2.getX()), Math.abs(p1.getY() - p2.getY()));
	}
	
	public int getParked()
	{
		return parked;	
	}
	
	public void initiateTest() {
    	    for (int i = 0; i < 12000*2; i+=2)
    	    {
    	    	double x,y;
				x = input[i];
				y = input[i+1];
    	    
    	    	Point p = new Point(x,y);
    	    	if(canPark(p)) {
    	    		parkedCars.add(p);
    	    		++parked;
    	    	}
    	    	
    	    	
    	    }
		
	}

}
