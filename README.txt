In order to execute the ParkingLot test program it is needed to provide files as parameters (at least 1) in the following format: ParkingLot <archive1> <archive2> … <archive n>. The test reads from each file 24000 real numbers (java double type), if less are provided the would not work properly. 

We also provide a RNG program also written in Java. In order to generate the file (generates 24000 real numbers) it is needed to provide an output file as a parameter, that is,  FileGenerator <archive1>. Furthermore this program can be adapted to generate your own random number files. 

Finally, to perform the Parking Lot Test some input files are already provided (see the files named: 1,2,3,4,5,6,7,8,9,10). To execute the test write in the console $java ParkingLot 1 2 3 4 5 6 7 8 9 10

PS: The programs provided are in Java language. To compile them write in the console 
$javac ProgramName.java To execute them write in the console $java ProgramName
(tested only with Java 8 version).
